package assignment;

public class Licenta implements Proiectable {

	private int nrPag;
	private String numeLicenta;
	
		public Licenta(int nrPag, String numeLicenta) {
		this.nrPag = nrPag;
		this.numeLicenta = numeLicenta;
	}

		@Override
		public void printNoOfPages() {
			System.out.println("This project has " + nrPag + " pages!" );
			
		}
	
	
}
